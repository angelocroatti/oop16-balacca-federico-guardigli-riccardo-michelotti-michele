package utilities;

import java.util.List;
import java.util.stream.Collectors;

import model.Garment;

public  class SearchImpl {
	

	/**
	 * this method search by type
	 * @param list
	 * @param typeDress
	 */
	public static List<Garment> searchByType(final List<Garment> list, final TypeDress typeDress) {
		
		
		 return  list.stream().filter(x -> x.getTypeDress() == typeDress).collect(Collectors.toList());
		
		
	}


	/**
	 * this method search by discount
	 * @param list
	 */
	public static List<Garment> searchByDiscount(final List<Garment> list) {
		
		
		
		return list.stream().filter(x -> x.getDiscount() == true).collect(Collectors.toList());
	}


	/**
	 * this method search by brand
	 * @param list
	 * @param brand
	 */
	public static List<Garment> searchByBrand(final List<Garment> list, final Brand brand) {
		
	    return list.stream().filter(x -> x.getBrand() == brand).collect(Collectors.toList());
	    
	}

	/**
	 * this method search by size
	 * @param list
	 * @param size
	 */
	public static List<Garment> searchBySize(final List<Garment> list, final DressSize size){
		
		return list.stream().filter(x -> x.getSize() == size).collect(Collectors.toList());
		
	}

	/**
	 * this method search by name
	 * @param list
	 * @param name
	 */
	public static List<Garment> searchByName(final List<Garment> list, final String name) {
		
		return list.stream().filter(x -> x.getName() == name).collect(Collectors.toList());
	
	}
	
	/**
	 * this method search by price
	 * @param list
	 * @param min
	 * @param max
	 */
	public static List<Garment> searchByPrice(final List<Garment> list, final double min, final double max) {
		
		return list.stream().filter(x -> x.getPrice() >= min).filter(x-> x.getPrice() <= max).collect(Collectors.toList());
	}
	
	/**
	 * this method search by shelf
	 * @param list
	 * @param num
	 */
	public static List<Garment> searchByShelf(final List<Garment> list, final int num){
		
		return list.stream().filter(x-> x.getShelfNum() == num).collect(Collectors.toList());
		
	}
}
