package utilities;

public enum ShopElement {
	
	SHELF, YOU_ARE_HERE, DOOR, CASH, PC, NONE; 
	
	private double angle = 0;
	
	public double getAngle(){
		return this.angle;
	}
	
	public void modifyAngle(){
		
		this.angle += 90;
		if(this.angle >= 360){
			this.angle = 0;
		}
	}
	
	public void setAngle(double angle){
		
		this.angle = angle;
	}
	
}
