package view.utils;

import java.io.IOException;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public final class Utils {

    private Utils() {}
    
    static double xOffset = 0;
    static double yOffset = 0;

    public final static void changeFxml(Node node,String FxmlFile,Object controller){
        try {
            final Stage stage=(Stage) node.getScene().getWindow();
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(controller);
            loader.setLocation(view.utils.Utils.class.getResource(FxmlFile));
            final Parent root = (Parent) loader.load();
            final Scene scene = new Scene(root);
            setBackground(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public final static void newWindow(String FxmlFile,Object controller){
        try {
            final Stage stage = new Stage();
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(controller);
            loader.setLocation(view.utils.Utils.class.getResource(FxmlFile));
            final Parent root1 = (Parent) loader.load();
            stage.setScene(new Scene(root1));
            stage.resizableProperty().setValue(Boolean.FALSE);
            if(FxmlFile.contains("Speech")){
                stage.initStyle(StageStyle.UNDECORATED);
                
                root1.setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        xOffset = stage.getX() - event.getScreenX();
                        yOffset = stage.getY() - event.getScreenY();
                        
                    }
                });
                
                root1.setOnMouseDragged(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        stage.setX(event.getScreenX() + xOffset);
                        stage.setY(event.getScreenY() + yOffset);
                    }
                });
            }
            
            stage.show();
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    if(FxmlFile.contains("Speech")){
                        Utils.playSound("SpeechOff.wav");  
                    }
                }
            }); 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public final static void loadSidebar(Node menu){
        VBox box;
        try {
            box = FXMLLoader.load(Utils.class.getResource("/view/menu/Menu.fxml"));
            ((Pane) menu).getChildren().setAll(box);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public final static void playSound(String file){
        final AudioClip audio = new AudioClip(Utils.class.getResource("/"+file).toString());
        audio.play();
    }

    public final static void alert(AlertType type,String title, String header, String context) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(context);
        alert.show();
    }
    
    public final static void setBackground(Parent root) {
        root.setStyle("-fx-background-image: url('" + "bg.jpg" + "'); " +
                "-fx-background-position: center center; " +
                "-fx-background-repeat: stretch;");
    }

    public final static void blurNodes(Node node, GaussianBlur blur) {
        node.setEffect(blur);
        if(node instanceof Parent) {
            Parent parent = (Parent) node;
            ObservableList<Node> children = parent.getChildrenUnmodifiable();
            for(Node child : children){
                blurNodes(child, blur);
            }
        }
    }

}
