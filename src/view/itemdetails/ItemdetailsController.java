package view.itemdetails;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import controller.ControllerImpl;
import controller.MapControllerImpl;
import controller.SerializerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.ShopImpl;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;
import view.email.EmailController;
import view.shopcreator.ShopMapImpl;
import view.utils.Utils;

public final class ItemdetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox vbox_content;

    @FXML
    private TitledPane tp_content;

    @FXML
    private ImageView img_item;

    @FXML
    private Label lbl_title;

    @FXML
    private Label lbl_url;

    @FXML
    private GridPane grd_shop;

    @FXML
    private JFXTextField txt_url;

    @FXML
    private JFXTextField txt_name;

    @FXML
    private JFXTextField txt_price;

    @FXML
    private JFXComboBox<DressSize> chb_size;

    @FXML
    private JFXComboBox<Brand> chb_brand;

    @FXML
    private JFXComboBox<TypeDress> chb_type;

    @FXML
    private JFXTextField txt_shelf;

    @FXML
    private JFXCheckBox chk_discount;

    @FXML
    private JFXButton btn_form;

    @FXML
    private JFXButton btn_form1;

    @FXML
    private VBox vbox_menu;

    private Boolean Admin=false;

    private int code;

    private int mode = 0; //0 : add   1 : change

    private boolean badurl=false;

    private ControllerImpl ci = ControllerImpl.getIstance();

    ShopMapImpl map = ShopMapImpl.getIstance();
    ShopImpl shop = ShopImpl.getIstance();
    MapControllerImpl manager = MapControllerImpl.getIstance();
    Pane[][] gridPane = new Pane[10][8];


    private static ItemdetailsController istance=null; //riferimento all' istanza


    private ItemdetailsController() {}//costruttore

    public static ItemdetailsController getIstance() {
        synchronized (ItemdetailsController.class) {
            if(istance==null){
                istance = new ItemdetailsController();
            }
        }
        return istance;
    }

    @FXML
    void initialize() {

        tp_content.heightProperty().addListener((obs, oldHeight, newHeight) -> tp_content.getScene().getWindow().sizeToScene());

        this.map = SerializerController.loadShopMap(map);
        manager.loadMap(this.gridPane, this.grd_shop,this.map);

        if(Admin) {
            isAdmin();
        }
        badurl=false;
        chb_type.getItems().setAll(TypeDress.values());
        chb_size.getItems().setAll(DressSize.values());
        chb_brand.getItems().setAll(Brand.values());
        //removes the last item from all the comboboxes 
        for (Node node : vbox_content.getChildren()) {
            if (node instanceof JFXComboBox) {
                ((JFXComboBox<?>) node).getItems().remove(((JFXComboBox<?>) node).getItems().size()-1);
            }
        }       
    }

    @FXML
    void sendForm(MouseEvent event) throws NumberFormatException, ClassNotFoundException, IOException {
        if(checkInputs()) {

            if(mode==0) {
                ci.createNewGarment(txt_name.getText(),Double.parseDouble(txt_price.getText()),chb_brand.getValue(),
                        chk_discount.isSelected(), Integer.parseInt(txt_shelf.getText()),chb_type.getValue(),
                        txt_url.getText(),chb_size.getValue());
            }
            if(mode==1) {
                ci.modifyGarmentToshop(txt_name.getText(), Double.parseDouble(txt_price.getText()), chb_brand.getValue(),
                        chk_discount.isSelected(), Integer.parseInt(txt_shelf.getText()), chb_type.getValue(),
                        txt_url.getText(), chb_size.getValue(),code);
            }

            Stage stage = (Stage) btn_form.getScene().getWindow();
            stage.hide();
        }
        else {
            Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Check the inputs!");
        }
    }

    @FXML
    void OpenMail(MouseEvent event) {
        EmailController ec = EmailController.getIstance();
        Utils.newWindow("/view/email/Email.fxml",ec);
        ec.setProd(txt_name.getText());

    }

    @FXML
    void updateImage(ActionEvent event) {
        try{
            URL u = new URL(txt_url.getText());
            HttpURLConnection huc =  (HttpURLConnection)  u.openConnection(); 
            huc.setRequestMethod("HEAD");
            huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
            huc.connect();

            System.out.println(huc.getResponseCode()+ huc.getHeaderField("Content-Type"));

            if(huc.getResponseCode()==404 || huc.getResponseCode()==403 
                    || huc.getResponseCode()==500 || (!huc.getHeaderField("Content-Type").contains("image")) )
            {
                System.err.println("Bad url");
                badurl=true;
                img_item.setImage(new Image("null.png"));
                Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Wrong url or resource not found!");
                Utils.playSound("error.wav");

            }
            else {
                img_item.setImage(new Image(txt_url.getText()));
            }
        }catch (Exception e) {
            Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Wrong url or resource not found!");
            Utils.playSound("error.wav");
        }
    }


    public void setTargetShelf(int num){

        manager.drawTargetShelf(num, gridPane);
    }

    private void isAdmin(){
        lbl_url.setOpacity(1);
        txt_url.setOpacity(1);
        chk_discount.setDisable(false);
        btn_form.setOpacity(1);
        btn_form.setDisable(false);
        btn_form1.setOpacity(1);
        btn_form1.setDisable(false);

        for (Node node : vbox_content.getChildren()) {
            if (node instanceof JFXTextField) {
                ((JFXTextField) node).setEditable(true);
                ((JFXTextField) node).setDisable(false);
            }
            if (node instanceof JFXComboBox) {
                ((JFXComboBox<?>) node).setDisable(false);
            }
        }    	
    }

    private boolean checkInputs() {
        boolean flag = true;
        for (Node node : vbox_content.getChildren()) {
            if (node instanceof JFXTextField) {
                if(((JFXTextField) node).getText() == null || (((JFXTextField) node).getText().trim().isEmpty())) {
                    flag=false;
                }
            }
            else {
                if(node instanceof JFXComboBox) {
                    if((((JFXComboBox<?>) node).getValue() == null)){
                        flag=false;
                    }
                }
            }

            if(badurl){
                txt_url.setText("null.png");
            }

        }
        return flag;
    }

    public void setImg_item(String url) {
        img_item.setImage(new Image(url));
        txt_url.setText(url);
    }

    public void setLbl_title(String title) {
        lbl_title.setText(title);
    }

    public void setTxt_name(String name) {
        txt_name.setText(name);
    }

    public void setTxt_price(String price) {
        txt_price.setText(price);
    }

    public void setChb_size(DressSize size) {
        chb_size.setValue(size);
    }

    public void setChb_brand(Brand brand) {
        this.chb_brand.setValue(brand);
    }

    public void setChb_type(TypeDress color) {
        chb_type.setValue(color);
    }

    public void setTxt_shelf(String value) {
        txt_shelf.setText(value);
    }

    public void setChk_discount(Boolean value) {
        chk_discount.setSelected(value);
    }

    public void setAdmin(Boolean admin) {
        Admin = admin;
    }

    public void setMode(int val) {
        mode = val;
    }

    public void setCode(long val) {
        code = (int) val;
    }

}
