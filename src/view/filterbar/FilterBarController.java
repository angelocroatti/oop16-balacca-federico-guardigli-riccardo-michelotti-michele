package view.filterbar;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;

import org.controlsfx.control.RangeSlider;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXToggleButton;
import javafx.fxml.FXML;
import javafx.scene.layout.FlowPane;
import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;
import view.utils.Utils;


public final class FilterBarController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    
    @FXML
    private FlowPane flp_advancedfilters;

    @FXML
    private RangeSlider sld_pricerange;

    @FXML
    private JFXCheckBox ckb_sale;

    @FXML
    private JFXComboBox<Brand> chb_filterbrand;

    @FXML
    private JFXComboBox<DressSize> chb_filtersize;

    @FXML
    private JFXComboBox<TypeDress> chb_filtertype;

    @FXML
    private JFXToggleButton tgl_filters;
    
    
    private static FilterBarController istance=null; //riferimento all' istanza

    private FilterBarController() {}//costruttore

    public static FilterBarController getIstance() {
        synchronized (FilterBarController.class) {
            if(istance==null){
                    istance = new FilterBarController();
            }
        }
        return istance;
    }
        
    @FXML
    void initialize() {
    	chb_filtertype.getItems().setAll(TypeDress.values());
    	chb_filtersize.getItems().setAll(DressSize.values());
    	chb_filterbrand.getItems().setAll(Brand.values());
    	chb_filterbrand.setValue(Brand.ANY);
    	resetFilters();
    }
      
    @FXML
    public void toggleFilters() {
    	Utils.playSound("click"+ThreadLocalRandom.current().nextInt(1, 3 + 1)+".wav");
    	if(!flp_advancedfilters.isDisabled()){
    		flp_advancedfilters.setDisable(true);
    		resetFilters();

    	}
    	else{
    		flp_advancedfilters.setDisable(false);
    		
    	}
    }
    
    public void enableFilters(){
    	    tgl_filters.setSelected(true);
    		flp_advancedfilters.setDisable(false);
    	}
    
    public void resetFilters() {
    	ckb_sale.setSelected(false);
    	chb_filterbrand.getSelectionModel().selectLast();
		chb_filtersize.getSelectionModel().selectLast();
		chb_filtertype.getSelectionModel().selectLast();
		sld_pricerange.setLowValue(0);
		sld_pricerange.setHighValue(100);
		
    	
    }

	public double getSld_pricerange_Min() {
		return Math.round(sld_pricerange.getLowValue());
	}
	
	public double getSld_pricerange_Max() {
		return Math.round(sld_pricerange.getHighValue());
	}

	public void setSld_pricerange(double min, double high) {
		sld_pricerange.setLowValue(min);
		sld_pricerange.setHighValue(high);
		
	}

	public boolean getCkb_sale() {
		return ckb_sale.isSelected();
	}

	public void setCkb_sale(Boolean value) {
		ckb_sale.setSelected(value);
	}

	public Brand getChb_filterbrand() {
		return chb_filterbrand.getValue();
	}

	public void setChb_filterbrand(Brand value) {
		chb_filterbrand.setValue(value);
	}

	public DressSize getChb_filtersize() {
		return chb_filtersize.getValue();
	}

	public void setChb_filtersize(DressSize value) {
		chb_filtersize.setValue(value);
	}

	public TypeDress getChb_filtertype() {
		return chb_filtertype.getValue();
	}

	public void setChb_filtertype(TypeDress value) {
		chb_filtertype.setValue(value);
	}
	
	public boolean isAdvfilterson() {
	    return tgl_filters.isSelected();
	}
}
