package view.client;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;

import controller.ControllerImpl;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import model.Garment;
import speech.controller.Speech_Controller;
import speech.controller.Speech_ControllerImpl;
import view.filterbar.FilterBarController;
import view.itemdetails.ItemdetailsController;
import view.speech.SpeechController;
import view.utils.Utils;


public final class ClientController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private BorderPane client_container;

	@FXML
	private VBox vbox_menu;

	@FXML
	private ScrollPane client_scrollpane;

	@FXML
	private Label client_label;

	@FXML
	private Label lbl_title;

	@FXML
	private BorderPane client_toppane;

	@FXML
	private JFXTextField client_txtbox;

	@FXML
	private JFXHamburger client_hamburger;

	@FXML
	private JFXDrawer client_drawer;

	@FXML
	private FlowPane client_filterbar;

	@FXML
	private FlowPane client_flowpane;

	@FXML
	private JFXButton btn_voice;
	
	List<Garment> garmentList;
	
    private FilterBarController fb = FilterBarController.getIstance();
    private ControllerImpl ci = ControllerImpl.getIstance();

	private static ClientController istance=null; //riferimento all' istanza

	private ClientController() {}//costruttore

	public static ClientController getIstance() {
	    synchronized (ClientController.class) {    
		    if(istance==null){
				istance = new ClientController();
			}
	    }
		return istance;
	}

	@FXML
	void initialize() throws ClassNotFoundException, IOException {
		
		btn_voice.setText("");
		ImageView iconmic = new ImageView(new Image("mic.png",true));
		iconmic.setFitHeight(32);
		iconmic.setFitWidth(32);
		btn_voice.setGraphic(iconmic);
		setup_fxmls();
		List<Garment> lol = ci.getGarmentList();
		populateView(lol);
	}

	@FXML
	public void search(ActionEvent event) {
	     garmentList = ci.multiSearch(fb.getChb_filtertype(), fb.getChb_filterbrand(),
	            fb.getChb_filtersize(), fb.getCkb_sale(), fb.getSld_pricerange_Min(), fb.getSld_pricerange_Max());
	     
	     client_flowpane.getChildren().clear();
	     
	     populateView(garmentList);
	}

	@FXML
	void startVoice(MouseEvent event) {
		Utils.playSound("SpeechOn.wav");	

		SpeechController sc = SpeechController.getIstance();
		Utils.newWindow("/view/speech/Speech.fxml",sc);
		try {
			//calling the controller of the speech for starting the voice
			Speech_Controller controller = Speech_ControllerImpl.getInstance();
			controller.enableVoiceRecognition();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void setup_fxmls(){

		Utils.loadSidebar(vbox_menu);

		BorderPane borderpane;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setController(fb);
			loader.setLocation(view.utils.Utils.class.getResource("/view/filterbar/FilterBar.fxml"));
			borderpane = loader.load();
			client_filterbar.getChildren().add(borderpane);
		} catch (IOException e1) {
			e1.printStackTrace();
		}  
	}

	void populateView(List<Garment> list){
		client_flowpane.setVgap(20);
		client_flowpane.setHgap(20);

		for(Garment i : list)
		{
			ImageView icon1;
			icon1 = new ImageView(new Image(i.getPhoto(),true));
			icon1.setCache(true);
			icon1.setFitHeight(150);
			icon1.setFitWidth(150);
			icon1.setPreserveRatio(true);
			icon1.setSmooth(true);

			Label label = new Label(i.getName()+"\n"+i.getPrice());
			label.setGraphic(icon1);
			label.setContentDisplay(ContentDisplay.TOP);

			client_flowpane.getChildren().add(label);

			label.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					ItemdetailsController ic = ItemdetailsController.getIstance();
    			    ic.setAdmin(false);
    				Utils.newWindow("/view/itemdetails/Itemdetails.fxml",ic);
    				ic.setCode(i.getCodeNum());
    				ic.setImg_item(i.getPhoto().toString());
                    ic.setTxt_name(i.getName());
                    ic.setTxt_price(String.valueOf(i.getPrice()));
                    ic.setChb_size(i.getSize());
                    ic.setChb_brand(i.getBrand());
                    ic.setChb_type(i.getTypeDress());
                    ic.setTxt_shelf(String.valueOf(i.getShelfNum()));
                    ic.setChk_discount(i.getDiscount());
                    ic.setTargetShelf(i.getShelfNum());
					}
				}
			);
		}
	}
}