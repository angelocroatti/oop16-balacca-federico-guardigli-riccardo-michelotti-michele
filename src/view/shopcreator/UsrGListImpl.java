package view.shopcreator;

import java.util.ArrayList;
import java.util.List;
import com.jfoenix.controls.JFXButton;
import controller.ControllerImpl;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Garment;
import view.itemdetails.ItemdetailsController;
import view.utils.Utils;




public class UsrGListImpl implements UsrGList{
	
		private ControllerImpl ci = ControllerImpl.getIstance();
		private static UsrGListImpl istance=null; //riferimento all' istanza
	
	
	    private UsrGListImpl() {}//costruttore
	
	    
	    public static UsrGListImpl getIstance() {
	    	
		    	synchronized (UsrGListImpl.class) {
		            	if(istance==null){
		            	
		                    	istance = new UsrGListImpl();
		            	}
		        }
		        
		        return istance;
	    }	  
			
		
	    public void garmentListView(List<Garment> list,FlowPane flp_list){
		    	
		    	clearFlb_list(flp_list);
				
				VBox vBox = new VBox();
				
				vBox.setAlignment(Pos.TOP_LEFT);
	
				
				for(Garment elem : list){
							
						BorderPane borderPane = new BorderPane();
						HBox hBox = new HBox();
						List<JFXButton> deletebutton = new ArrayList<>();
						JFXButton buttonModify = new JFXButton("Modify");
						JFXButton buttonDelete = new JFXButton("Delete");
						deletebutton.add(new JFXButton("Delete"));

						buttonDelete.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
							
								@Override
								public void handle(MouseEvent event) {
									
										ci.deleteGarmentToShop(elem.getCodeNum());								
								};
							
						});

						
						buttonModify.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
							
								@Override
								public void handle(MouseEvent event) {
			                        ItemdetailsController itc = ItemdetailsController.getIstance();
	                                itc.setAdmin(true);
	                                itc.setMode(1);
			                        view.utils.Utils.newWindow( "/view/itemdetails/Itemdetails.fxml",itc);
			                        itc.setCode(elem.getCodeNum());
			                        itc.setImg_item(elem.getPhoto().toString());
			                        itc.setTxt_name(elem.getName());
			                        itc.setTxt_price(String.valueOf(elem.getPrice()));
			                        itc.setChb_size(elem.getSize());
			                        itc.setChb_brand(elem.getBrand());
			                        itc.setChb_type(elem.getTypeDress());
			                        itc.setTxt_shelf(String.valueOf(elem.getShelfNum()));
			                        itc.setChk_discount(elem.getDiscount());
			                        itc.setTargetShelf(elem.getShelfNum());
									
								};
							
						});
					
						
						ImageView image;
						image = new ImageView(new Image(elem.getPhoto(),true));
						image.setCache(true);
						image.setFitHeight(150);
						image.setFitWidth(150);
						image.setPreserveRatio(true);
						image.setSmooth(true);
		
						Label label = new Label(elem.getName()+"\nIdCode: " + elem.getCodeNum());
						label.setGraphic(image);
						label.setContentDisplay(ContentDisplay.BOTTOM);
						
						
						label.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
								@Override
								public void handle(MouseEvent event) {
									
										ItemdetailsController ic = ItemdetailsController.getIstance();
					    			    ic.setAdmin(false);
					    				Utils.newWindow("/view/itemdetails/Itemdetails.fxml",ic);
					    				ic.setCode(elem.getCodeNum());
					    				ic.setImg_item(elem.getPhoto().toString());
					                    ic.setTxt_name(elem.getName());
					                    ic.setTxt_price(String.valueOf(elem.getPrice()));
					                    ic.setChb_size(elem.getSize());
					                    ic.setChb_brand(elem.getBrand());
					                    ic.setChb_type(elem.getTypeDress());
					                    ic.setTxt_shelf(String.valueOf(elem.getShelfNum()));
					                    ic.setChk_discount(elem.getDiscount());
					                    ic.setTargetShelf(elem.getShelfNum());
								}								
						});
						
						
						borderPane.setCenter(label);
						
						
						borderPane.setBottom(hBox);
						hBox.getChildren().add(buttonModify);
						hBox.getChildren().add(buttonDelete);
						vBox.getChildren().add(borderPane);
						flp_list.getChildren().add(borderPane);					
				}
		}
		 
	    
	  
		private void clearFlb_list(FlowPane flp_list){
		    	
		    	flp_list.getChildren().clear();
		}
		 
		 
	 
}
