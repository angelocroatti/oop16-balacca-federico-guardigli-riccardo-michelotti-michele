package view.shopcreator;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import controller.ControllerImpl;
import controller.MapControllerImpl;
import controller.SerializerController;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.Shelf;
import model.ShopImpl;
import view.itemdetails.ItemdetailsController;
import view.utils.Utils;
import utilities.SearchImpl;
import utilities.ShopElement;




public final class ShopCreatorController {
	
	
	
		private final static String ALL_SHELF = "ALL";
	    @FXML
	    private GridPane grd_shop;
	    @FXML
	    private JFXButton btn_insertgarment;
	    @FXML
	    private JFXToggleButton tgl_editor;
	    @FXML
	    private JFXButton btn_search;
	    @FXML
	    private ChoiceBox<String> chb_shelfs;
	    @FXML
	    private FlowPane flp_list;    
	    @FXML
	    private VBox vbox_menu;    
	    @FXML
	    private TextField txt_xcoord;
	    @FXML
	    private TextField txt_ycoord;
	    @FXML
	    private JFXButton btn_Insert;
	    @FXML
	    private JFXButton btn_rotate;
	    @FXML
	    private JFXButton btn_delete;  
	    @FXML
	    private TextField shelfXCoord;
	    @FXML
	    private TextField shelfYCoord;
	    @FXML
	    private VBox vbx_modifier;   
	    @FXML
	    private ChoiceBox<ShopElement> csb_element;
	    
	    private Pane[][] gridPane = new Pane[10][8];
	    
	    ShopMapImpl map = ShopMapImpl.getIstance();
	    ControllerImpl ci = ControllerImpl.getIstance();
	    ShopImpl shop = ShopImpl.getIstance();
	    MapControllerImpl manager = MapControllerImpl.getIstance();
	    UsrGListImpl flowPane = UsrGListImpl.getIstance();
	    
	    private int x;
	    private int y;
	    
	    private static ShopCreatorController istance=null; //riferimento all' istanza
	
	    
	    
	    private ShopCreatorController() {}//costruttore
	    
	
	    public static ShopCreatorController getIstance() {
	    	
		        synchronized (ShopCreatorController.class) {
		            
		        		if(istance==null){
		        		
		                    	istance = new ShopCreatorController();		                    
		        		}
		        }
		        
		        return istance;		        
		    }
	
		/**
		 * load view
		 */
	    @FXML
	    public void initialize() {
	    	
		    	Utils.loadSidebar(vbox_menu);
		    	
		    	this.csb_element.getItems().addAll(ShopElement.values());
		    	
		    	chbShelfSet();
		    	
		    	deserializeMap();
		    	
		    	manager.loadMap(gridPane, grd_shop, map);   
		    	
		    	flowPane.garmentListView(ci.getGarmentList(), flp_list);	
		    	
	
	    }
	    
	    
	    @FXML
	    public void activeEditor(MouseEvent event) {
	    	
		    	Utils.playSound("click"+ThreadLocalRandom.current().nextInt(1, 3 + 1)+".wav");
		    	
		    	if(this.vbx_modifier.isDisabled()){
		    		
		    			this.vbx_modifier.setDisable(false);
		    			
		    	}else{
		    		
		    			this.vbx_modifier.setDisable(true);		    		
		    	}	
	    }
	    

	    public void deserializeMap(){
	    	
	    		this.map = SerializerController.loadShopMap(map);
	    }
	    
	    
	
	
	    
	    @FXML
	    public void insertGarment(MouseEvent event) {
		
		    	ItemdetailsController itc = ItemdetailsController.getIstance();
		    	
		    	itc.setAdmin(true);
		    	
		    	itc.setMode(0);
		    	
		    	view.utils.Utils.newWindow( "/view/itemdetails/Itemdetails.fxml",itc);		    	
	    }
	    
	
	    @FXML
	    public void insertElement(MouseEvent event) throws ClassNotFoundException, IOException {
	    	
		    	if(setCoord()){
		    	
			    		this.manager.addNewElement(x, y, csb_element.getValue(), gridPane, map);
			    		
			    		this.manager.setAngleOfElement(x, y, gridPane, map);
			    		
			    		this.gridPane[x][y].setVisible(true);
			    		
			    		if(map.getElementFromMap(x, y) == ShopElement.SHELF){
			    			
			    			chbShelfSet();
			    		}
			    		
			    		saveMapShop(); 
			    		ci.saveShop();
		    	}
	    } 
	        
	    
	    @FXML
	    public void RotateElement(MouseEvent event) {
	    	    	
		    	if(setCoord()){
				    	this.map.getShopMap()[x][y].modifyAngle();
				    	
				    	this.manager.setAngleOfElement(x, y, gridPane, map);
				    	
				    	saveMapShop();
		    	}
	
	    }
	    
	    
	    @FXML
	    public void deleteElement(MouseEvent event) {
	    	
		    	if(setCoord()){
			    		
			    		this.manager.deleteElementOnMap(x, y, gridPane, map);
			    		
			    		chbShelfSet();
			    		
			    		refreshMap();			    		
			    		saveMapShop();
			    		ci.saveShop();			    		
		    	}    	
	    }
	    
	    
	    @FXML
	    public void searchToShelf(MouseEvent event) {
	    	
				this.shelfXCoord.clear();
				this.shelfYCoord.clear();
		    	
		    	if(this.chb_shelfs.getValue() == ALL_SHELF){
			    		
			    		refreshMap();
			    		
			    		flowPane.garmentListView(ci.getGarmentList(), flp_list);
			    		
		    	}else{
		    		
				    	int nShelf = Integer.parseInt(this.chb_shelfs.getValue());
				    	
				    	refreshMap();
				    	
				    	this.manager.drawTargetShelf(nShelf, gridPane);
				    	
				    	flowPane.garmentListView(SearchImpl.searchByShelf(ci.getGarmentList(), nShelf), flp_list);
				    	
				    	for(Shelf shelf : ci.getShelfList()){
				    		
					    		if(shelf.getNShelf() == nShelf){
					
						    			this.shelfXCoord.setText(Integer.toString(shelf.getPosXShelf()+1));
						    			this.shelfYCoord.setText(Integer.toString(shelf.getPosYShelf()+1));
					    		}
				    	}
		    	}
	    }
	    
	    
	    public FlowPane getFlp_list(){
		   
	    	return this.flp_list;
	   }
	    
	    
	    private boolean setCoord(){
	    	
		    	try{
		    		
		    			this.x = Integer.parseInt(this.txt_xcoord.getText())-1;
		    	    
		    	}catch(Exception e){
		    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of X is not Number!");
			    		return false;		    	    
		    	  }
		    	
		    	if(this.x < 0){
		    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of X is too Smaller!");
			    		return false;
		    	}
		    	
		    	if( this.x > 9){
			    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of X is too Greater!");
			    		return false;
		    	}
		    	
		    	
		    	try{
		    		
		    	    	this.y = Integer.parseInt(this.txt_ycoord.getText())-1;
		    	    
		    	}catch(Exception e){
		    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of Y is not Number!");
			    		return false;		    	    
		    	  }
		    	
		    	if(this.y < 0){
		    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of Y is too Smaller!");
			    		return false;
		    	}
		    	
		    	if( this.y > 7){
		    		
			    		Utils.alert(AlertType.ERROR, "ERROR", "ERROR", "Value of Y is too Greater!");
			    		return false;
		    	}
		    	
		    	return true;
	    }
	       
	    
	    private void insertShelfNum(String num){
	    	
	    		this.chb_shelfs.getItems().add(num);
	    }
	    
	    
	    private void chbShelfSet(){
	    	
		    	this.chb_shelfs.getItems().clear();
		    	
		    	insertShelfNum(ALL_SHELF);
		    	
		    	for(Shelf shelf : ci.getShelfList()){
		    		
		    			insertShelfNum(Integer.toString(shelf.getNShelf()));
		    	}	    	
	    }
	    
	    
	    private void saveMapShop() {
	    	
	    		SerializerController.saveShopMap(map);	    	
	    }    
	    
	    
	    private void refreshMap(){
	    	
	    		this.grd_shop.getChildren().clear();
	    		this.manager.loadMap(gridPane, grd_shop, map);
	    }
	    
}





