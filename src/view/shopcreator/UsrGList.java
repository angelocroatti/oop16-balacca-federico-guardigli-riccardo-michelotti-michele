package view.shopcreator;

import java.util.List;

import javafx.scene.layout.FlowPane;
import model.Garment;

public interface UsrGList {
	
	/**
	 * this method show on the flowPane in shopCreator the list of garment
	 * @param list
	 * @param flp_list
	 */
	public void garmentListView(List<Garment> list,FlowPane flp_list);
	

}
