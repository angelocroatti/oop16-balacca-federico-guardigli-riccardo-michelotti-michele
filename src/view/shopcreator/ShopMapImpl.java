package view.shopcreator;

import java.io.Serializable;

import utilities.ShopElement;

public class ShopMapImpl implements ShopMap, Serializable {

	/**
	 * 
	 */
		private static final long serialVersionUID = 1L;
		double[][] agleMap;
		ShopElement[][] map;
		private static ShopMapImpl istance=null;
		
		
		private ShopMapImpl() {
			
				this.map = new ShopElement[10][8];
				this.agleMap = new double[10][8];
				
				for(int i = 0; i < 10; i++ ){
					
						for(int j = 0; j < 8; j++){
		        	
								this.map[i][j] = ShopElement.NONE;
								this.agleMap[i][j] = 0;			
						}
				}			
		}
		
	
	    public static ShopMapImpl getIstance() {
	    	
		        synchronized (ShopMapImpl.class) {
		            
		        		if(istance==null){
		        		
		                    	istance = new ShopMapImpl();                    
		        		}
		        }
		        
		        return istance;
	    }
		
	    
		@Override
		public ShopElement[][] getShopMap() {
			
				return this.map;				
		}
	
		
		@Override
		public void setElementOnMap(int x, int y, ShopElement elem) {
			
				this.map[x][y] = elem;	
		}
	
	
		@Override
		public ShopElement getElementFromMap(int x, int y) {
			
				return this.map[x][y];				
		}
	
	
		@Override
		public double[][] getAngleMap() {
			
				return this.agleMap;		
		}
	
		
		@Override
		public double getAngleElement(int x, int y) {
			
				return this.agleMap[x][y];				
		}
	
	
		@Override
		public void setAngleElement(int x, int y, double angle) {
			
				this.agleMap[x][y] = angle;			
		}

}
