package main;

import java.io.IOException;

import controller.ControllerImpl;
import controller.SerializerController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import speech.controller.Speech_Controller;
import speech.controller.Speech_ControllerImpl;
import speech.model.SpeechModel;
import speech.model.SpeechModelImpl;


public class Preloader extends javafx.application.Preloader{

	@Override
	public void start(Stage primaryStage) throws Exception {

		final FXMLLoader loader = new FXMLLoader();
		loader.setLocation(view.utils.Utils.class.getResource("/view/preloader/Preloader.fxml"));
		try{
			Parent root = (Parent) loader.load();
			final Scene scene = new Scene(root,400,400);
			primaryStage.setScene(scene);
			primaryStage.setMinWidth(400);
			primaryStage.setMinHeight(400);
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}


		ControllerImpl ci = ControllerImpl.getIstance();
		SerializerController.stampa();
		ci.loadShop();
		final SpeechModel model = SpeechModelImpl.getInstance();
		final Speech_Controller controller= Speech_ControllerImpl.getInstance();
		controller.setModel(model);
		primaryStage.close();

	}}

