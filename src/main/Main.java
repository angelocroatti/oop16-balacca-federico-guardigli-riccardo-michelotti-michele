package main;
	
import java.io.IOException;

import com.sun.javafx.application.LauncherImpl;

import controller.ControllerImpl;
import controller.SerializerController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import view.client.ClientController;
import view.utils.Utils;


public class Main extends Application {
	@Override
	public void start(final Stage primaryStage) {
		
		SerializerController.stampa();
			ControllerImpl ci = ControllerImpl.getIstance();
			System.out.println(ci.getGarmentList().size());
			System.out.println(ci.getShelfList().size());
			ClientController cc = ClientController.getIstance();
			final FXMLLoader loader = new FXMLLoader();
		    loader.setController(cc);
		    loader.setLocation(view.utils.Utils.class.getResource("/view/client/Client.fxml"));
            try {
                Parent root = (Parent) loader.load();
                final Scene scene = new Scene(root,800,600);
                primaryStage.setScene(scene);
                primaryStage.setMinWidth(810);
                primaryStage.setMinHeight(610);
                primaryStage.setTitle("ShopAssistant");
                primaryStage.resizableProperty().setValue(Boolean.FALSE);
                Utils.setBackground(root);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
			
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                	Platform.exit();
                }
            });
	}
	public static void main(String[] args) {
	    LauncherImpl.launchApplication(Main.class, Preloader.class, args);
	}
}