package model;

public interface IdCodeCounter {
	
	/**
	 * this method is used to get the progressive code of a field
	 * @return current code
	 */
	 long getCode();

	 /**
	  * this method increase of 1 unit the progressive code
	  */
	 void nextCode();

}
